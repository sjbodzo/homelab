#!/usr/bin/bash -xe

# Chill Zoom: https://gist.github.com/abraithwaite/1d78a946f90be478faedb5ca4db6d62e
cat <<EOF > "${HOME}/.config/systemd/user/zoom.slice"
[Slice]
AllowedCPUs=0-4
MemoryHigh=6G
EOF

cat /usr/share/applications/Zoom.desktop | sed -E 's#^(Exec=).*$#Exec=/usr/bin/systemd-run --user --slice=zoom.slice /opt/zoom/ZoomLauncher#' > "${HOME}/.local/share/applications/Zoom.desktop"

update-desktop-database ~/.local/share/applications
