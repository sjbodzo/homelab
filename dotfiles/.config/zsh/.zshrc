autoload -U compinit; compinit
_comp_options+=(globdots)

eval "$(starship init zsh)"
eval `ssh-agent`

ssh-add $HOME/.ssh/github_id_rsa
ssh-add $HOME/.ssh/gitlab_id_rsa

# . $ZDOTDIR/zsh-autosuggestions/zsh-autosuggestions.zsh
. $ZDOTDIR/.zsh-history-substring-search/history-substring-search.plugin.zsh
. $ZDOTDIR/.zsh-vi-mode/zsh-vi-mode.plugin.zsh
. $ZDOTDIR/.zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh

# Configure command lookup
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Fix Java Swing apps (like Jetbrains IDE) for Sway
export _JAVA_AWT_WM_NONREPARENTING=1

export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/go/bin"
export PATH="$HOME/.poetry/bin:$PATH"

# Display fix for Wayland with external display
export WLR_DRM_NO_MODIFIERS=1

